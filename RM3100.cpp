/*!
 *  @file RM3100.cpp
 *
 *  @mainpage PNI RM3100 geomagnetic sensors.
 *
 *  @section intro_sec Introduction
 *
 *  This is a library for the PNI RM3100 geomagnetic sensors.
 *
 *  @section author Author
 *
 *  Written by Igor Afonkin.
 */

#include "RM3100.h"

//=============================================================================
/*
 * @brief Class constructor
 */
RM3100::RM3100( uint8_t param, boolean iface ) {
  _iface = iface;
  if (_iface == RM3100_SPI) {
    // configure as SPI slave
    _spiSS = param;

    pinMode(_spiSS, OUTPUT);
    digitalWrite(_spiSS, HIGH);

    _spi = SPI;
    _spi.begin();
  } else {
    // configure as I2C slave
    _i2cAddress = param;
  }
}
//=============================================================================
/*!
 *  @brief  Setup sensor pins
 */
uint8_t RM3100::begin() {
  uint8_t _ret;
  //DEBUG_PRINTLN("Initialize begin...");
  DEBUG_PRINTLN(F("Read revision ID."));
  if (_iface == RM3100_SPI) {
    // for SPI
    _spi.beginTransaction(SPISettings(SPI_SPEED, MSBFIRST, SPI_MODE0));
    digitalWrite(_spiSS, LOW);
    delayMicroseconds(1);

    _ret = spiTransfer(RM3100_REG::REVID | 0x80); // send command to read REVID
    DEBUG_PRINT(_ret);
    DEBUG_PRINT(':');
    _ret = spiTransfer(); // read RM3100_REG_REVID
    DEBUG_PRINTLN(_ret);

    digitalWrite(_spiSS, HIGH);
    _spi.endTransaction();
  } else {
    // for I2C
    _ret = _i2cAddress;
  }
  //DEBUG_PRINTLN("Initialize end...");
  return _ret;
}
//=============================================================================
boolean RM3100::writeCycleCountReg(uint16_t ccx, uint16_t ccy, uint16_t ccz) {
  uint8_t _tmp;
  uint16_t _ccr;
  boolean _ret = true;
  DEBUG_PRINTLN(F("Write Cycle Count Registers."));
  if (_iface == RM3100_SPI) {
    // for SPI
    _spi.beginTransaction(SPISettings(SPI_SPEED, MSBFIRST, SPI_MODE0));
    digitalWrite(_spiSS, LOW);
    delayMicroseconds(1);

    spiTransfer(RM3100_REG::CCX);
    spiTransfer((ccx >> 8) & 0xFF);
    spiTransfer((ccx & 0xFF));
    spiTransfer((ccy >> 8) & 0xFF);
    spiTransfer(ccy & 0xFF);
    spiTransfer((ccz >> 8) & 0xFF);
    spiTransfer(ccz & 0xFF);

    digitalWrite(_spiSS, HIGH);
    delay(1);
    digitalWrite(_spiSS, LOW);
    delayMicroseconds(1);

    spiTransfer(RM3100_REG::CCX | 0x80);
    _tmp = spiTransfer();
    _ccr = (_tmp << 8) | spiTransfer();
    if ( _ccr != ccx ) _ret = false;
    _tmp = spiTransfer();
    _ccr = (_tmp << 8) | spiTransfer();
    if ( _ccr != ccy ) _ret = false;
    _tmp = spiTransfer();
    _ccr = (_tmp << 8) | spiTransfer();
    if ( _ccr != ccz ) _ret = false;

    digitalWrite(_spiSS, HIGH);
    _spi.endTransaction();
  }
  return _ret;
}
//=============================================================================
/*
RM3100::CCR RM3100::readCycleCountReg() {
  CCR _ret;
  uint8_t _tmp;
  DEBUG_PRINTLN(F("Read Cycle Count Registers:"));
  if (_iface == RM3100_SPI) {
    // for SPI
    _spi.beginTransaction(SPISettings(SPI_SPEED, MSBFIRST, SPI_MODE0));
    digitalWrite(_spiSS, LOW);
    delayMicroseconds(1);

    _tmp = spiTransfer(RM3100_REG::CCX | 0x80); // send command to read REVID
    DEBUG_PRINT(_tmp);
    DEBUG_PRINT(':');
    _tmp = spiTransfer(0xFF); // read RM3100_REG_CCX
    _ret.CCX = (_tmp << 8) | spiTransfer(0xFF); // read RM3100_REG_CCX
    DEBUG_PRINT(_ret.CCX);
    DEBUG_PRINT('\t');
    _tmp = spiTransfer(0xFF); // read RM3100_REG_CCY
    _ret.CCY = (_tmp << 8) | spiTransfer(0xFF); // read RM3100_REG_CCY
    DEBUG_PRINT(_ret.CCY);
    DEBUG_PRINT('\t');
    _tmp = spiTransfer(0xFF); // read RM3100_REG_CCZ
    _ret.CCZ = (_tmp << 8) | spiTransfer(0xFF); // read RM3100_REG_CCZ
    DEBUG_PRINTLN(_ret.CCZ);

    digitalWrite(_spiSS, HIGH);
    _spi.endTransaction();
  }
  return _ret;
}
*/
//=============================================================================
void RM3100::startContinuousMeasurement(uint8_t _tmrc) {
  _spi.beginTransaction(SPISettings(SPI_SPEED, MSBFIRST, SPI_MODE0));
  digitalWrite(_spiSS, LOW);
  delayMicroseconds(1);

  // set TRMC
  spiTransfer(RM3100_REG::TMRC);
  spiTransfer(_tmrc);

  digitalWrite(_spiSS, HIGH);
  delay(1);
  digitalWrite(_spiSS, LOW);
  delayMicroseconds(1);

  // init continuous measurement mode
  spiTransfer(RM3100_REG::CMM);
  // data ready after a full measurement sequence is completed
  spiTransfer(0b01110001);

  _spi.endTransaction();
  digitalWrite(_spiSS, HIGH);
}
//=============================================================================
/*
boolean RM3100::waitResults() {
  _spi.beginTransaction(SPISettings(SPI_SPEED, MSBFIRST, SPI_MODE0));
  digitalWrite(_spiSS, LOW);
  delayMicroseconds(1);

  spiTransfer(RM3100_REG::STATUS | 0x80);
  uint8_t _ret = spiTransfer();

  _spi.endTransaction();
  digitalWrite(_spiSS, HIGH);
  if ((_ret & 0x80) == 0) return false;
  return true;
}
*/
//=============================================================================
boolean RM3100::readResults() {
  _spi.beginTransaction(SPISettings(SPI_SPEED, MSBFIRST, SPI_MODE0));
  digitalWrite(_spiSS, LOW);
  delayMicroseconds(1);

  spiTransfer(RM3100_REG::STATUS | 0x80);
  boolean _ret = ( (spiTransfer() & 0x80) != 0x00 );

  if ( _ret ) {
    // if data ready then read into raw buffer
    digitalWrite(_spiSS, HIGH);
    delay(1);
    digitalWrite(_spiSS, LOW);
    delayMicroseconds(1);

    spiTransfer(RM3100_REG::MX | 0x80);
    rawData[0] = spiTransfer();
    rawData[1] = spiTransfer();
    rawData[2] = spiTransfer();
    rawData[3] = spiTransfer();
    rawData[4] = spiTransfer();
    rawData[5] = spiTransfer();
    rawData[6] = spiTransfer();
    rawData[7] = spiTransfer();
    rawData[8] = spiTransfer();
  }

  digitalWrite(_spiSS, HIGH);
  _spi.endTransaction();
  return _ret;
}
//=============================================================================
long RM3100::convert2long(uint8_t h, uint8_t m, uint8_t l) {
  long _ret = 0L;
  if ((h & 0x80) == 0) {
    // if positive
    _ret = (_ret | h) << 8;
    _ret = (_ret | m) << 8;
    _ret |= l;
  } else {
    // if negative
    h = ~h; m = ~m; l = ~l;
    _ret = (_ret | h) << 8;
    _ret = (_ret | m) << 8;
    _ret |= l;
    _ret = -_ret - 1;
  }
  return _ret;
}
//=============================================================================
