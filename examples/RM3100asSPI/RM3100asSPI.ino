/*
 * Test sketch for the my RM3100 library
 */
//#include <ESP8266WiFi.h>
#include <SPI.h>
#include <RM3100.h>

#define LED_PERIOD 1000

#ifdef __AVR
  #define LED LED_BUILTIN
  #define CS1 8
#else
  #define LED 2 // for NodeMCU
  #define CS1 D8
#endif

RM3100 geo1(CS1);

unsigned long lastMillis;

//=============================================================================
void setup() {
  pinMode(LED, OUTPUT);

  Serial.begin(9600);
  Serial.println();
  Serial.println(F("Sketch started..."));

  lastMillis = millis();

  geo1.begin();
  if ( geo1.writeCycleCountReg(1200, 1200, 1200) ) {
    Serial.println(F("Cycle count registers succesfully set"));
    geo1.startContinuousMeasurement(0x9A);
  } else {
    Serial.println(F("Can't set cycle count registers"));
  }

  Serial.println(F("End of setup..."));
}
//=============================================================================
void loop() {
  if ((millis() - lastMillis) > LED_PERIOD) {
    digitalWrite(LED, !digitalRead(LED));
    lastMillis = millis();
  }
  if ( geo1.readResults() ) {
    // data ready
    Serial.print(geo1.getMX()); Serial.print('\t');
    Serial.print(geo1.getMY()); Serial.print('\t');
    Serial.println(geo1.getMZ());

  }
}
//=============================================================================
