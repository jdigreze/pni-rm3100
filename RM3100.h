/*!
 *  @file RM3100.h
 *
 *  This is a library for PNI RM3100 geomagnetic sensors.
 *
 *  Written by Igor Afonkin.
 *
 */

#ifndef __RM3100_H__
#define __RM3100_H__

#define SPI_SPEED 1000000
#define RM3100_SPI true
#define RM3100_I2C false

#define RM3100_REG_POLL   0x00
#define RM3100_REG_CMM    0x01
#define RM3100_REG_CCX    0x04
#define RM3100_REG_CCY    0x06
#define RM3100_REG_CCZ    0x08
#define RM3100_REG_TMRC   0x0B
#define RM3100_REG_MX     0x24
#define RM3100_REG_MY     0x27
#define RM3100_REG_MZ     0x2A
#define RM3100_REG_BIST   0x33
#define RM3100_REG_STATUS 0x34
#define RM3100_REG_HSHAKE 0x35
#define RM3100_REG_REVID  0x36

#include <Arduino.h>
#include <SPI.h>

/* Uncomment to enable printing out nice debug messages. */
#define RM3100_DEBUG

#define DEBUG_PRINTER Serial /**< Define where debug output will be printed. */

/* Setup debug printing macros. */
#ifdef RM3100_DEBUG
  #define DEBUG_PRINT(...) { DEBUG_PRINTER.print(__VA_ARGS__); }
  #define DEBUG_PRINTLN(...) { DEBUG_PRINTER.println(__VA_ARGS__); }
#else
  #define DEBUG_PRINT(...) {} /**< Debug Print Placeholder if Debug is disabled */
  #define DEBUG_PRINTLN(...) {} /**< Debug Print Line Placeholder if Debug is disabled */
#endif

//=============================================================================
/*!
 *  @brief  Class that stores state and functions for RM3100
 */
class RM3100 {
  public:
    RM3100(uint8_t param, boolean iface = RM3100_SPI);
    enum RM3100_REG : uint8_t {
        POLL   = 0x00,
        CMM    = 0x01,
        CCX    = 0x04,
        CCY    = 0x06,
        CCZ    = 0x08,
        TMRC   = 0x0B,
        MX     = 0x24,
        MY     = 0x27,
        MZ     = 0x2A,
        BIST   = 0x33,
        STATUS = 0x34,
        HSHAKE = 0x35,
        REVID  = 0x36
    };
/*
    typedef struct {
      uint16_t CCX;
      uint16_t CCY;
      uint16_t CCZ;
    } CCR;
*/
    uint8_t begin();
    boolean writeCycleCountReg(uint16_t, uint16_t, uint16_t);
    //CCR readCycleCountReg();
    void startContinuousMeasurement(uint8_t);
    //void stopContinuousMeasurement();
    //boolean waitResults();
    boolean readResults();
    long getMX() { return convert2long(rawData[0], rawData[1], rawData[2]); };
    long getMY() { return convert2long(rawData[3], rawData[4], rawData[5]); };
    long getMZ() { return convert2long(rawData[6], rawData[7], rawData[8]); };
  private:
    uint8_t _spiSS;
    uint8_t _i2cAddress;
    boolean _iface;
    SPIClass _spi;
    uint8_t rawData[9];
    uint8_t spiTransfer(uint8_t b = 0xFF) { return _spi.transfer(b); };
    long convert2long(uint8_t, uint8_t, uint8_t);
};

#endif
//=============================================================================
